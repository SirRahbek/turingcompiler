NAME = turing

ifeq ($(TYPE),debug)
	CFLAGS = -O0 -std=c++11
else
	CFLAGS = -s -O3 -std=c++11
endif

all:
	c++ $(NAME).cpp -o $(NAME) $(CFLAGS)

install:
	install $(NAME) /usr/bin/$(NAME)