#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

class Transition;
typedef map<string,Transition> State;


class Transition
{
public:
	string symbol;    // The replacement symbol
	char   direction; // +(next), -(before), 0(stay)
	string state;     // Next state

	Transition (string sy, char dir, string st) : symbol(sy), direction(dir), state(st) {return;};

	Transition (const Transition& copy) {
		symbol    = copy.symbol;
		direction = copy.direction;
		state     = copy.state;
		return;
	}
};



class StateMachine
{
protected:
	map<string, State*> states;
	State* currentState;

	FILE* f;
	char* line;
	char* symbol;
	char* replace;
	char  direction;
	char* state;

public:
	StateMachine (string input) {
		f       = fopen(input.c_str(), "r");
		line    = (char*) malloc(1000);
		symbol  = (char*) malloc(100);
		replace = (char*) malloc(100);
		state   = (char*) malloc(100);

		//add accept, reject and start state first.
		states["accept"] = new State();
		states["reject"] = new State();
		states["start"]  = new State();

		currentState = states["start"];

		while ( (line=fgets(line,1000,f)) != NULL ) {
			int n=0;
			for (  ; line[n] == '\t' || line[n] == ' ' ; n++) 
				continue; //forward beyond tabs and spaces

			//cut of the newline in the line.
			for (int i=0 ; line[i] !=0 ; i++)
				if (line[i] == '\n')
					line[i] = 0;

			//skip empty lines
			if (strlen(&line[n]) <= 0)
				continue;

			//skip comments
			if (line[n] == '#')
				continue;
			
			//start new state
			if (line[n] == ':') {
				//TODO: search for name in map first. no duplicates.
				if (!states.count(&line[n+1]))
					states[string(&line[n+1])] = new State();

				currentState = states[&line[n+1]];
				continue;
			}

			sscanf(&line[n], "%s %s %c %s", symbol, replace, &direction, state);
			if (currentState != NULL)
				currentState->emplace(symbol, Transition(replace, direction, state));				
		}

		fclose(f);
		free(line);
		free(symbol);
		free(replace);
		free(state);
		return;
	}

	~StateMachine () {
		for (auto st : states)
			delete st.second;

		return;
	}

	string compile () {
		string output;

		for (auto st : states) {
			output += st.first + ":\n";

			//accept and reject should only return 0 or 1.
			if (st.first == "accept") {
				output += "\tmov rax, 0\n";
				output += "\tret\n";
				continue;
			}
			if (st.first == "reject") {
				output += "\tmov rax, 1\n";
				output += "\tret\n";
				continue;
			}

			int count = 1;
			for (auto t : *st.second) {
				//Ignore kleene transition.
				if (t.first == "*")
					continue;

				output += string("\tcmp byte [rax], `") + t.first + "`\n";

				output += string("\tjne .T") + to_string(count) + "\n";
				output += string("\tmov byte [rax], `") + t.second.symbol + "`\n";
				
				if (t.second.direction == '+')
					output += string("\tinc rax") + "\n";
				if (t.second.direction == '-')
					output += string("\tdec rax") + "\n";

				output += string("\tjmp ") + t.second.state.c_str() + "\n";
				output += string("\t.T") + to_string(count) + ":\n";
				count++;
			}

			//if there is a kleene transition, use that.
			if (st.second->count("*")) {
				Transition t = st.second->at("*");
				if (t.symbol != "*")
					output += string("\tmov byte [rax], `") + t.symbol + "`\n";
				
				if (t.direction == '+')
					output += string("\tinc rax") + "\n";
				if (t.direction == '-')
					output += string("\tdec rax") + "\n";

				output += string("\tjmp ") + t.state.c_str() + "\n";
			}
			else {
				output += "\tinc ax\n";
				output += string("\tjmp ") + st.first + "\n";
			}
		}
		return output;
	}
};



int main (int argc, char* argv[])
{
	if ( access( "/usr/bin/nasm", F_OK ) == -1 ) {
		printf("This program needs to have nasm installed\n");
		return 1;
	}
	if (argc < 2) {
		cout << "We need more arguments, Turing is not pleased.\n";
		return 1;
	}

	string input = "";
	bool keep_assembly = false;

	for (int i=0 ; i<argc ; i++) {
		if (strcmp(argv[i],"-a") == 0)
			keep_assembly = true;
		else
			input = argv[i];
	}


	StateMachine machine(input+".t");

	ofstream outfile;
	outfile.open(input+".s");

	outfile << "section .text\n";
	outfile << "\tglobal _start\n";
	outfile << "_start:\n";
	//this takes all the data from stdin, to the tape.
	outfile << "\tmov rax, 0\n";
	outfile << string("\tmov rdi, ") + to_string(STDIN_FILENO) + "\n";
	outfile << "\tmov rsi, tape_2\n";
	outfile << "\tmov rdx, 10000\n";
	outfile << "\tsyscall\n";
	//move tape pointer to rax, and transfer control.
	outfile << "\tmov rax, tape_2\n";
	outfile << "\tcall start\n";
	outfile << "\tmov [return], ax\n";
	//move to start of readable output
	outfile << "\tmov rax, tape_1\n";
	outfile << "\t.scroll:\n";
	outfile << "\tcmp byte [rax], 0x1F\n"; //visible chars start at 0x20
	outfile << "\tja .end_scroll\n";
	outfile << "\tinc rax\n";
	outfile << "\tjmp .scroll\n";
	outfile << "\t.end_scroll:\n";
	//move to end of output to get size
	outfile << "\tmov rdx, rax\n";
	outfile << "\t.scroll_2:\n";
	outfile << "\tcmp byte [rdx], 0x1F\n"; //visible chars start at 0x20
	outfile << "\tjb .end_scroll_2\n";
	outfile << "\tinc rdx\n";
	outfile << "\tjmp .scroll_2\n";
	outfile << "\t.end_scroll_2:\n";
	outfile << "\tsub rdx, rax\n";
	//move the tape to stdout.
	outfile << "\tmov rsi, rax\n";
	outfile << "\tmov rax, 1\n";
	outfile << string("\tmov rdi, ") + to_string(STDOUT_FILENO) + "\n";
	outfile << "\tsyscall\n";
	//return from the program
	outfile << "\tmov rdi, [return]\n";
	outfile << "\tmov rax, 60\n";
	outfile << "\tsyscall\n";
	//include the rest of the code
	outfile << machine.compile();
	outfile << "\n\nsection .data\n";
	outfile << "\treturn: dw 0\n";
	//NOTE: tape is split in two. Code starts halfway.
	outfile << "\ttape_1: times 10000 db 0\n";
	outfile << "\ttape_2: times 10000 db 0\n";
	
	outfile.close();
	
	string nasm_command = string("nasm -felf64 ") + argv[1] + ".s";
	string ld_command   = string("ld ") + argv[1]+".o -o" + argv[1];
	string rm_assembly  = string("rm ") + argv[1]+".s "; 
	string rm_object    = string("rm ") + argv[1]+".o"; 
	int x; //x just prevents compiler warnings.
	x = system( nasm_command.c_str() );
	x = system( ld_command.c_str() );
	x = system( rm_object.c_str() );
	if (!keep_assembly)
		x = system( rm_assembly.c_str() );
	return 0;
}
