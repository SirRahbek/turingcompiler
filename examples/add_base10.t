#This turing machine adds two numbers in the format
# 1234x1234
# in base 10
#and can take arbitrairy length inputs.

:start
	* * 0 run_right

:take
	1 0 - run_left
	2 1 - run_left
	3 2 - run_left
	4 3 - run_left
	5 4 - run_left
	6 5 - run_left
	7 6 - run_left
	8 7 - run_left
	9 8 - run_left
	0 9 - take
	+ + 0 clean
	* * 0 run_left

:add
	0 1 0 run_right
	1 2 0 run_right
	2 3 0 run_right
	3 4 0 run_right
	4 5 0 run_right
	5 6 0 run_right
	6 7 0 run_right
	7 8 0 run_right
	8 9 0 run_right
	9 0 - add
	* 1 0 run_right

:run_right
	0 0 + run_right
	1 1 + run_right
	2 2 + run_right
	3 3 + run_right
	4 4 + run_right
	5 5 + run_right
	6 6 + run_right
	7 7 + run_right
	8 8 + run_right
	9 9 + run_right
	+ + + run_right
	* * - take

:run_left
	0 0 - run_left
	1 1 - run_left
	2 2 - run_left
	3 3 - run_left
	4 4 - run_left
	5 5 - run_left
	6 6 - run_left
	7 7 - run_left
	8 8 - run_left
	9 9 - run_left
	+ + - add
	* * 0 reject

:clean
	\0 \0 0 accept
	 * \0 + accept