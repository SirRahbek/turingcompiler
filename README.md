# TuringCompiler

This is a simple compiler, that turns descriptions of turing machines into machine code that will run on 64bit Linux machines.
The internal tape has a size of ~20kb, and loads the contents from stdin, into the tape center at start, and runs from there.

The syntax is pretty straightforward.

:state
  x y d state
  ...
  
You define states by prepending a line with a colon.
The transitions are defines in 4-tuples as such: <tape-symbol> <replace-symbol> <direction> <state>
This will replace the tape-symbol with the replace-symbol, move in the desired direction, to the
desired state.
A direction can be either +,- or 0, for forward, backwards and stay.
every state can hold a wildcard transition, that looks like follows: * * d state.

And well, the language is turing-complete i guess, so have fun porting Doom and Linux.
